import "./App.css";
import MessageList from "./components/MessageList";
import MessageInput from "./components/MessageInput";
import { useState } from "react";

function App() {
  return (
    <div>
      <div className="Header">Incognito Chat</div>
      <MessageList />
      <MessageInput />
    </div>
  );
}

export default App;
