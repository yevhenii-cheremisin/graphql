//import "./MessageList.css";
import React, { useState } from "react";
import "./Message.css";
import ReplyInput from "./ReplyInput";

function Message({ message }) {
  return (
    <div className="container">
      <div className="messageBlock">
        <div className="messageBlock-header">
          <span>{message.id}</span>
        </div>
        <div className="messageBlock-main">
          <span>{message.text}</span>
        </div>
        <div className="messageBlock-replies">
          {message.replies
            ? message.replies.map((repl) => {
                return (
                  <span key={repl.id} className="reply">
                    {repl.text}
                  </span>
                );
              })
            : null}
        </div>
        <ReplyInput messageId={message.id} />
      </div>
      <div className="messageBlock-likes">
        <span>{message.likes}</span>
        <button>Like</button>
        <button>Disike</button>
      </div>
    </div>
  );
}

export default Message;
