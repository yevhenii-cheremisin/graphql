import React, { useState } from "react";
import "./MessageInput.css";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

function MessageInput(props) {
  const [text, setMessage] = useState("");

  const query = gql`
    mutation NewMes($text: String!) {
      postMessage(text: $text) {
        id
        text
      }
    }
  `;

  const [mutation, { data }] = useMutation(query);

  const sendMessageOnClick = () => {
    mutation({ variables: { text } });
    setMessage("");
  };

  return (
    <div className="message-input">
      <input
        className="message-input-text"
        onChange={(e) => setMessage(e.target.value)}
        value={text}
        type="text"
      />
      <button className="message-input-button" onClick={sendMessageOnClick}>
        Send
      </button>
    </div>
  );
}

export default MessageInput;
