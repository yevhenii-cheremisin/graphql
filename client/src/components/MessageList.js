//import "./MessageList.css";
import React, { useState } from "react";
import Message from "./Message";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";

function MessageList() {
  const query = gql`
    {
      messages {
        messageList {
          id
          text
          likes
          replies {
            id
            text
          }
        }
      }
    }
  `;
  const { data, loading } = useQuery(query);

  return (
    <div>
      {!loading ? (
        data.messages.messageList.map((message) => {
          return <Message key={message.id} message={message} />;
        })
      ) : (
        <h1>loading...</h1>
      )}
    </div>
  );
}
export default MessageList;
