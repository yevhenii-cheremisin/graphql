import React, { useState } from "react";
import "./ReplyInput.css";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

function ReplyInput({ messageId }) {
  const [text, setMessage] = useState("");

  const query = gql`
    mutation NewMut($messageId: ID!, $text: String!) {
      postReply(messageId: $messageId, text: $text) {
        id
        text
      }
    }
  `;

  const [mutation, { data }] = useMutation(query);

  const sendMessageOnClick = () => {
    mutation({ variables: { messageId, text } });
    setMessage("");
  };

  return (
    <div className="reply-input">
      <input
        className="reply-input-text"
        onChange={(e) => setMessage(e.target.value)}
        value={text}
        type="text"
      />
      <button className="reply-input-button" onClick={sendMessageOnClick}>
        Send
      </button>
    </div>
  );
}

export default ReplyInput;
