function postMessage(parent, args, context) {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
  });
}

async function postReply(parent, args, context, info) {
  const messageExist = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExist) {
    throw new Error(`message with ID ${args.messageId} does not exists`);
  }

  return context.prisma.createReply({
    text: args.text,
    likes: 0,
    message: { connect: { id: args.messageId } },
  });
}

module.exports = {
  postMessage,
  postReply,
};
